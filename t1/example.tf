provider "gitlab" {
    token = "${token}"
}

resource "gitlab_project" "sample_project" {
    name = "terraform1"
    visibility_level = "public"
}

resource "gitlab_deploy_key" "sample_deploy_key" {
    can_push = "true"
    project = "${gitlab_project.sample_project.id}"
    title = "terraform_example_key"
    key = "ssh-rsa AAAAAjyUoPStYgxObTIgvoasAnWxUgnDqraMg8DD8sR42ganvdeP3quD9gJf6wtGGB2a1WZLWwdLr4n4SuWX"
}
