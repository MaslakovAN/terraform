variable "token" {}

provider "vscale" {
  token = "${var.token}"
}

resource "vscale_ssh_key" "MaslakovAN_key" {
  name = "MaslakovAN_pub_key"
  key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "vscale_scalet" "web" {
  make_from  = "centos_7_64_001_master"
  rplan = "small"
  name   = "MaslakovAN_vds"
  location = "msk0"
  ssh_keys = ["${vscale_ssh_key.MaslakovAN_key.id}", "29174"]

}
