variable "vsc_token" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_zone_id" {}

provider "vscale" {
  token = "${var.vsc_token}"
}

resource "vscale_scalet" "web" {
  make_from  = "centos_7_64_001_master"
  rplan = "small"
  name   = "MaslakovAN_vds"
  location = "msk0"
  ssh_keys = ["29236", "29429"]
}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-1"
}

resource "aws_route53_record" "www" {
  zone_id = "${var.aws_zone_id}"
  name    = "fleix-kent.devops.rebrain.srwx.net"
  type    = "A"
  ttl     = "300"
  records = ["${vscale_scalet.web.public_address}"]
}
