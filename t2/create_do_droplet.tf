variable "token" {}

provider "digitalocean" {
  token = "${var.token}"
}

resource "digitalocean_ssh_key" "fist_key" {
  name       = "MaslakovAN_pub_key"
  public_key = "${file("~/.ssh/id_rsa.pub")}"
}

resource "digitalocean_droplet" "web" {
  image  = "ubuntu-18-04-x64"
  name   = "web-MaslakovAN"
  region = "ams3"
  size   = "s-1vcpu-1gb"
  tags   = ["MaslakovAN"]
  ssh_keys = ["${digitalocean_ssh_key.fist_key.fingerprint}", "24042393"]

}
